import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   public static void main (String[] param) {
      String rpn = "2 5 9 ROT + SWAP -";
      Tnode res = buildFromRPN(rpn);
      System.out.println(res);
   }

   public Tnode(String name) {
      this.name = name;
   }

   @Override
   public String toString() {
      StringBuffer strong = new StringBuffer();
      strong.append(name);
      if (firstChild != null) {
         strong.append("(");
         strong.append(firstChild.toString());
      }
      if (nextSibling != null) {
         strong.append(",");
         strong.append(nextSibling.toString());
         strong.append(")");
      }
      return strong.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      Stack<Tnode> stack = new Stack<>();
      String[] array = pol.split(" ");
      Tnode root = null;
      for (String c : array) {
         for (String charr : array) {
            if (!charr.matches("[+\\-*/]|.*\\d+.*|SWAP|ROT")) throw new RuntimeException(
                    "Incorrect number/char " + charr + " in " + pol);
         }
         if (c.length() > 0) {
            root = new Tnode(c);
            if (c.matches("[+\\-*/]")) {
               try {
                  root.nextSibling = stack.pop();
                  root.firstChild = stack.pop();
               } catch (Exception e) {throw new RuntimeException("No elements in " + pol);}

            }
            else if (c.equals("SWAP")) {
               Tnode first = stack.pop();
               Tnode second = stack.pop();
               stack.push(first);
               stack.push(second);
               continue;
            }
            else if (c.equals("ROT")) {
               stack.push(stack.remove(stack.size() - 3));
               continue;
            }
            stack.push(root);
         }
      }
      if (stack.isEmpty()) throw new RuntimeException("No elements in " + pol);
      if (stack.size() > 1) throw new RuntimeException(
              "There are too many numbers and not enough operators in " + stack);
      return root;
   }
}